import {
  SAVE_USER_NAME,
  SAVE_USER_LOCATION,
  SAVE_USER_LANGUAGE,
  ADD_USER_SKILL,
  DELETE_USER_SKILL
} from '../constants/index';

// Card info by default
const defaultUserState = {
  name: 'User Name',
  location: 'GB, London',
  language: 'English',
  skills: [
    { name: 'ReactJS', level: 10 },
    { name: 'Redux', level: 8 },
    { name: 'JavaScript', level: 9 },
    { name: 'CSS', level: 7 }
  ]
};

export default (userState = defaultUserState, action) => {
  const { type, payload } = action;
  switch (type) {
    // Save user name
    case SAVE_USER_NAME:
      return { ...userState, name: payload };
    // Save user location
    case SAVE_USER_LOCATION:
      return { ...userState, location: payload };
    // Save user language
    case SAVE_USER_LANGUAGE:
      return { ...userState, language: payload };
    // Add new skill
    case ADD_USER_SKILL:
      const newSkills = userState.skills.filter(
        skill => skill.name !== payload.name
      );
      return { ...userState, skills: [...newSkills, payload] };
    // Delete skill
    case DELETE_USER_SKILL:
      return {
        ...userState,
        skills: userState.skills.filter(skill => payload !== skill.name)
      };
    // By default
    default:
      return userState;
  }
};
