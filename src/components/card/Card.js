import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  saveUserName,
  saveUserLocation,
  saveUserLanguage,
  addUserSkill,
  deleteUserSkill
} from '../../actions/';
import InputField from '../inputField/Inputfield';
import Skills from '../skill/Skills';
import './Card.css';
import {
  Portfolio,
  Experience,
  Sample,
  Availability,
  Amazing,
  Clients,
  Blank,
  Note
} from '../more/';

class Card extends React.Component {
  static propTypes = {
    card: PropTypes.shape({
      name: PropTypes.string,
      location: PropTypes.string,
      skills: PropTypes.array
    }),
    saveUserName: PropTypes.func.isRequired,
    saveUserLocation: PropTypes.func.isRequired
  };
  render() {
    const { name, location, language } = this.props.card;
    return (
      <div className="container">
        <div className="item photo">
          <img src={require('../../assets/images/photo.png')} alt={name} />
        </div>
        <div className="item about">
          <div>
            <InputField
              value={name}
              onSave={this.saveName}
              label={'Enter your name'}
              class={'form-name'}
            />
          </div>
          <div>
            <InputField
              value={location}
              onSave={this.saveLocation}
              label={'Enter your location'}
              class={'form-location'}
            />
          </div>
          <div>
            <InputField
              value={language}
              onSave={this.saveLanguage}
              label={'Enter your language'}
              class={'form-language'}
            />
          </div>
          <Skills
            skills={this.props.card.skills}
            onDelete={this.handlerDelete}
          />
          <div>
            <InputField
              value={'Add skills'}
              onSave={this.saveSkill}
              isSkill={true}
              emptyOnClick={true}
              label={'Add your skill'}
              class={'form-skill'}
            />
          </div>
        </div>
        <Portfolio />
        <Experience />
        <Sample />
        <Availability />
        <Amazing />
        <Clients />
        <Blank />
        <Note />
      </div>
    );
  }
  /**
   * Save name
   */
  saveName = name => {
    const { saveUserName } = this.props;
    saveUserName(name);
  };
  /**
   * Save location
   */
  saveLocation = location => {
    const { saveUserLocation } = this.props;
    saveUserLocation(location);
  };
  saveLanguage = language => {
    const { saveUserLanguage } = this.props;
    saveUserLanguage(language);
  };
  /**
   * Save new skill
   */
  saveSkill = (name, level) => {
    const { addUserSkill } = this.props;
    addUserSkill(name, level);
  };
  /**
   * Delete skill
   */
  handlerDelete = key => {
    const { deleteUserSkill } = this.props;
    deleteUserSkill(key);
  };
}

// Экспорт компонента
export default connect(
  state => {
    return {
      card: state.card
    };
  },
  {
    saveUserName,
    saveUserLocation,
    saveUserLanguage,
    addUserSkill,
    deleteUserSkill
  }
)(Card);
