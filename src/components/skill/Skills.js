import React from 'react';
import PropTypes from 'prop-types';
import Skill from './Skill';
import './Skills.css';

/**
 * Show list of skills
 */
class Skills extends React.Component {
  static proptypes = {
    skills: PropTypes.array.isRequired,
    onDelete: PropTypes.func.isRequired
  };

  render() {
    const { skills, onDelete } = this.props;
    let list = 'No skills';
    if (skills.length > 0) {
      list = skills.map(skill => (
        <Skill key={skill.name} skill={skill} onDelete={onDelete} />
      ));
    }
    return <div className="skills">{list}</div>;
  }
}

export default Skills;
