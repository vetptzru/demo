import React from 'react';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';
import './Skill.css';

/**
 * Show skill
 */
class Skill extends React.Component {
  static proptypes = {
    skill: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired
  };

  render() {
    const { skill } = this.props;
    return (
      <div key={skill.name} data-skil={skill.skil} className="skill">
        {skill.name}
        <span
          className="delete-btn"
          onClick={() => this.handlerDelete(skill.name)}
        >
          <FontAwesome name="times" />
        </span>
      </div>
    );
  }
  handlerDelete(key) {
    this.props.onDelete(key);
  }
}

export default Skill;
