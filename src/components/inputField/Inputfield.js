import React from 'react';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';
import './InputField.css';
import { SKILLS_LIST } from '../../constants/';

/**
 * Component for showing editable input field
 */
class InputField extends React.Component {
  // State of component
  state = {
    isEdit: false,
    currentValue: '',
    currentSkillLevel: 1
  };
  // Default properties of component
  static defaultProps = {
    isSkill: false,
    defaultSkillLevel: 1,
    emptyOnClick: false,
    minSize: 1,
    maxSize: 20,
    class: ''
  };
  // Types of proporties
  static propTypes = {
    value: PropTypes.string.isRequired,
    onSave: PropTypes.func.isRequired,
    isSkill: PropTypes.bool,
    emptyOnClick: PropTypes.bool,
    label: PropTypes.string.isRequired,
    minSize: PropTypes.number,
    maxSize: PropTypes.number,
    class: PropTypes.string
  };
  /**
   * Render input fields
   */
  render() {
    let body = '';
    if (this.state.isEdit) {
      body = (
        <div className="input-form">
          <input
            placeholder={this.props.label}
            type="text"
            defaultValue={this.props.emptyOnClick ? '' : this.props.value}
            onChange={this.handlerChange}
            onKeyDown={this.handlerKeyPressed}
            autoFocus
          />
          <div className="form-panel">
            {this.props.isSkill && (
              <select
                value={this.state.currentSkillLevel}
                onChange={this.handlerSelectChange}
              >
                {SKILLS_LIST.map(skill => (
                  <option value={skill.value}>{skill.name}</option>
                ))}
              </select>
            )}
            <span>
              <FontAwesome
                name="check-circle"
                className="input-field-accept"
                onClick={this.handlerAccept}
              />
            </span>
            <span>
              <FontAwesome
                name="times-circle"
                className="input-field-denied"
                onClick={this.handlerDenied}
              />
            </span>
          </div>
        </div>
      );
    } else {
      body = (
        <div onClick={this.handlerClick} className={'edit-box'}>
          {this.props.value}
        </div>
      );
    }
    return <div className={this.props.class}>{body}</div>;
  }
  /**
   * Event handler on field click
   */
  handlerClick = () => {
    if (!this.state.isEdit) {
      this.setState({
        ...this.state,
        isEdit: true,
        currentValue: this.props.emptyOnClick ? '' : this.props.value,
        currentSkillLevel: this.props.defaultSkillLevel
      });
    }
  };
  /**
   * Call save action from properies
   */
  saveInput() {
    if (
      this.state.currentValue.length >= this.props.minSize &&
      this.state.currentValue.length <= this.props.maxSize
    ) {
      this.setState({
        ...this.state,
        isEdit: false
      });
      this.props.onSave(this.state.currentValue, this.state.currentSkillLevel);
    }
  }
  /**
   * Event handler on key press
   */
  handlerKeyPressed = ev => {
    if (ev.keyCode === 13) {
      this.saveInput();
    } else if (ev.keyCode === 27) {
      this.setState({
        ...this.state,
        isEdit: false
      });
    }
  };
  /**
   * Accept saving (button)
   */
  handlerAccept = () => {
    this.saveInput();
  };
  /**
   * Event handler of changing field
   */
  handlerChange = ev => {
    this.setState({
      ...this.state,
      currentValue: ev.target.value
    });
  };
  /**
   * Event handler for removing chnges
   */
  handlerDenied = () => {
    this.setState({
      ...this.state,
      isEdit: false
    });
  };
  /**
   * Event handler of changing select inputs
   */
  handlerSelectChange = ev => {
    this.setState({
      ...this.state,
      currentSkillLevel: ev.target.value
    });
  };
}

export default InputField;
