import React from 'react';

export default function() {
  return (
    <div className="item more">
      <div className="title">
        <h4>Experience</h4>
      </div>
      <ul>
        <li>
          <strong>PHP</strong>, 6 years
        </li>
        <li>
          <strong>Ruby</strong>, 7 years
        </li>
        <li>
          <strong>Javascript</strong>, 4 years
        </li>
        <li>
          <strong>ActionScript</strong>, 3 years
        </li>
      </ul>
    </div>
  );
}
