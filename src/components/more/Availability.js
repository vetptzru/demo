import React from 'react';

export default function() {
  return (
    <div className="item more double">
      <div className="title">
        <h4>Availability</h4>
      </div>
      <h2>Full-time</h2>
      <div className="title bordered">
        <h3>Preferred environment</h3>
      </div>
      <p>Git, GitHub, vim, emacs, Jenkins, Mac OSX</p>
    </div>
  );
}
