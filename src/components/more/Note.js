import React from 'react';

export default function() {
  return (
    <div className="item more">
      <div className="title">
        <h4>Note</h4>
      </div>
      <blockquote>
        … Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua et dolore
        magna aliqua et dol ut labore et dolore
      </blockquote>
    </div>
  );
}
