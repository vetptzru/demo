import React from 'react';

export default function() {
  return (
    <div className="item more">
      <div className="title">
        <h4>Portfolio</h4>
        PHP, Ruby, Javascript
      </div>
      <ul>
        <li>
          <strong>NavalPlan</strong>, PHP, Ruby
        </li>
        <li>
          <strong>MyTime</strong>, Javascript
        </li>
        <li>
          <strong>Formidable</strong>, PHP, Ruby
        </li>
        <li>
          <strong>MyTime</strong>, Javascript
        </li>
        <li>
          <strong>Monsoon</strong>, ActionScript
        </li>
      </ul>
    </div>
  );
}
