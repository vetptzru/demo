import React from 'react';

export default function() {
  return (
    <div className="item more code">
      <div className="title">
        <h4>Sample code and algorithms</h4>
      </div>
    </div>
  );
}
