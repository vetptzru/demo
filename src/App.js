import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store/';
import './App.css';
import Card from './components/card/Card';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="wrapper">
          <Card />
        </div>
      </Provider>
    );
  }
}

export default App;
