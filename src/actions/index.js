import {
  SAVE_USER_NAME,
  SAVE_USER_LOCATION,
  SAVE_USER_LANGUAGE,
  ADD_USER_SKILL,
  DELETE_USER_SKILL
} from '../constants/index';

export function saveUserName(name) {
  return {
    type: SAVE_USER_NAME,
    payload: name
  };
}

export function saveUserLocation(location) {
  return {
    type: SAVE_USER_LOCATION,
    payload: location
  };
}

export function saveUserLanguage(language) {
  return {
    type: SAVE_USER_LANGUAGE,
    payload: language
  };
}

export function addUserSkill(name, level) {
  return {
    type: ADD_USER_SKILL,
    payload: { name, level }
  };
}

export function deleteUserSkill(key) {
  return {
    type: DELETE_USER_SKILL,
    payload: key
  };
}
