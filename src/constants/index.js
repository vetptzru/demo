export const SAVE_USER_NAME = 'SAVE_USER_NAME';
export const SAVE_USER_LOCATION = 'SAVE_USER_LOCATION';
export const SAVE_USER_LANGUAGE = 'SAVE_USER_LANGUAGE';
export const ADD_USER_SKILL = 'ADD_USER_SKILL';
export const DELETE_USER_SKILL = 'DELETE_USER_SKILL';
export const SKILLS_LIST = [
  { name: 'low', value: 1 },
  { name: 'normal', value: 2 },
  { name: 'hight', value: 3 },
  { name: 'strong', value: 4 }
];
